<?php

/**
 * @file
 * openafpm_site.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function openafpm_site_user_default_roles() {
  $roles = array();

  // Exported role: webadmin.
  $roles['webadmin'] = array(
    'name' => 'webadmin',
    'weight' => 3,
  );

  // Exported role: worker.
  $roles['worker'] = array(
    'name' => 'worker',
    'weight' => 2,
  );

  return $roles;
}
