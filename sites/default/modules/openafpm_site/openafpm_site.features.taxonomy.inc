<?php

/**
 * @file
 * openafpm_site.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function openafpm_site_taxonomy_default_vocabularies() {
  return array(
    'forums' => array(
      'name' => 'Discussion Forum',
      'machine_name' => 'forums',
      'description' => 'Forum navigation vocabulary',
      'hierarchy' => 1,
      'module' => 'forum',
      'weight' => -10,
    ),
  );
}
