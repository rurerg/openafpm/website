<?php

/**
 * @file
 * openafpm_site.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function openafpm_site_default_rules_configuration() {
  $items = array();
  $items['rules_get_one'] = entity_import('rules_config', '{ "rules_get_one" : {
      "LABEL" : "Get one",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "php", "rules" ],
      "ACCESS_EXPOSED" : "1",
      "USES VARIABLES" : {
        "worker_id" : { "label" : "Worker ID", "type" : "text" },
        "nid" : { "label" : "nid", "type" : "integer", "parameter" : false },
        "simulation_type" : { "label" : "simulation_type", "type" : "text", "parameter" : false }
      },
      "DO" : [
        { "php_eval" : { "code" : "variable_set(\\u0027ntua_wind_last_ping\\u0027, time());" } },
        { "entity_query" : {
            "USING" : {
              "type" : "node",
              "property" : "field_simulation_status",
              "value" : "1",
              "limit" : "1"
            },
            "PROVIDE" : { "entity_fetched" : { "simulation" : "simulation" } }
          }
        },
        { "component_rules_set_in_queue" : { "node" : [ "simulation:0" ] } },
        { "component_rules_get_sim_type" : {
            "USING" : { "node" : [ "simulation:0" ] },
            "PROVIDE" : { "simulation_type" : { "node_simulation_type" : "simulation_type" } }
          }
        },
        { "data_set" : { "data" : [ "nid" ], "value" : [ "simulation:0:nid" ] } },
        { "data_set" : { "data" : [ "simulation-type" ], "value" : [ "node-simulation-type" ] } }
      ],
      "PROVIDES VARIABLES" : [ "nid", "simulation_type" ]
    }
  }');
  $items['rules_get_sim_type'] = entity_import('rules_config', '{ "rules_get_sim_type" : {
      "LABEL" : "Get sim type",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "node" : { "label" : "node", "type" : "node" },
        "simulation_type" : { "label" : "simulation_type", "type" : "text", "parameter" : false }
      },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_simulation_type" } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "simulation-type" ],
            "value" : [ "node:field-simulation-type" ]
          }
        }
      ],
      "PROVIDES VARIABLES" : [ "simulation_type" ]
    }
  }');
  $items['rules_go_to_sim_list'] = entity_import('rules_config', '{ "rules_go_to_sim_list" : {
      "LABEL" : "Go to sim list",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_insert--simulation" : { "bundle" : "simulation" },
        "node_update--simulation" : { "bundle" : "simulation" },
        "node_insert--optiafpm_tool" : { "bundle" : "optiafpm_tool" },
        "node_update--optiafpm_tool" : { "bundle" : "optiafpm_tool" },
        "node_update--userafpm_tool" : { "bundle" : "userafpm_tool" },
        "node_insert--userafpm_tool" : { "bundle" : "userafpm_tool" }
      },
      "DO" : [ { "redirect" : { "url" : "my-simulation-list" } } ]
    }
  }');
  $items['rules_new_user_notification'] = entity_import('rules_config', '{ "rules_new_user_notification" : {
      "LABEL" : "New user notification",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_insert" : [] },
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "4" : "4" } },
            "subject" : "New users registered",
            "message" : "There are new users pending activation.\\r\\n"
          }
        }
      ]
    }
  }');
  $items['rules_ping'] = entity_import('rules_config', '{ "rules_ping" : {
      "LABEL" : "Ping",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "php", "rules" ],
      "ACCESS_EXPOSED" : "1",
      "USES VARIABLES" : { "worker_id" : { "label" : "Worker ID", "type" : "text" } },
      "DO" : [
        { "php_eval" : { "code" : "variable_set(\\u0027ntua_wind_last_ping\\u0027, time());" } }
      ]
    }
  }');
  $items['rules_rerun_failed'] = entity_import('rules_config', '{ "rules_rerun_failed" : {
      "LABEL" : "Rerun failed",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ACTION SET" : [
        { "entity_query" : {
            "USING" : {
              "type" : "node",
              "property" : "field_simulation_status",
              "value" : "4",
              "limit" : "10000"
            },
            "PROVIDE" : { "entity_fetched" : { "entity_fetched" : "Fetched entity" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "entity-fetched" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : [ { "entity_save" : { "data" : [ "list-item" ] } } ]
          }
        }
      ]
    }
  }');
  $items['rules_rerun_failed_single_'] = entity_import('rules_config', '{ "rules_rerun_failed_single_" : {
      "LABEL" : "Rerun simulation",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "node", "type" : "node" } },
      "ACTION SET" : [ { "entity_save" : { "data" : [ "node" ] } } ]
    }
  }');
  $items['rules_reset_simulation'] = entity_import('rules_config', '{ "rules_reset_simulation" : {
      "LABEL" : "Reset simulation",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_update--simulation" : { "bundle" : "simulation" },
        "node_update--optiafpm_tool" : { "bundle" : "optiafpm_tool" },
        "node_update--userafpm_tool" : { "bundle" : "userafpm_tool" }
      },
      "IF" : [
        { "data_is" : {
            "data" : [ "node-unchanged:field-simulation-status" ],
            "op" : "IN",
            "value" : { "value" : { "0" : "0", "4" : "4" } }
          }
        },
        { "data_is" : { "data" : [ "node-unchanged:status" ], "value" : [ "node:status" ] } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-simulation-status" ], "value" : "1" } },
        { "data_set" : { "data" : [ "node:field-variable-file" ], "value" : [ "" ] } },
        { "data_set" : { "data" : [ "node:field-json-result" ] } },
        { "data_set" : { "data" : [ "node:field-simulation-messages" ] } },
        { "data_set" : { "data" : [ "node:field-femm-image" ], "value" : [ "" ] } }
      ]
    }
  }');
  $items['rules_reset_simulation_clone'] = entity_import('rules_config', '{ "rules_reset_simulation_clone" : {
      "LABEL" : "Reset simulation (clone)",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "clone" ],
      "ON" : { "clone_node" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "cloned-node" ], "field" : "field_simulation_status" } },
        { "entity_has_field" : { "entity" : [ "cloned-node" ], "field" : "field_json_result" } },
        { "entity_has_field" : { "entity" : [ "cloned-node" ], "field" : "field_variable_file" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "cloned-node:field-simulation-status" ], "value" : "1" } },
        { "data_set" : { "data" : [ "cloned-node:field-json-result" ] } },
        { "data_set" : { "data" : [ "cloned-node:field-variable-file" ], "value" : [ "" ] } },
        { "entity_save" : { "data" : [ "cloned-node" ], "immediate" : "1" } }
      ]
    }
  }');
  $items['rules_reset_simulation_new_'] = entity_import('rules_config', '{ "rules_reset_simulation_new_" : {
      "LABEL" : "Reset simulation (new)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_insert--simulation" : { "bundle" : "simulation" },
        "node_insert--userafpm_tool" : { "bundle" : "userafpm_tool" },
        "node_insert--optiafpm_tool" : { "bundle" : "optiafpm_tool" }
      },
      "DO" : [
        { "data_set" : { "data" : [ "node:field-simulation-status" ], "value" : "1" } },
        { "data_set" : { "data" : [ "node:field-variable-file" ], "value" : [ "" ] } },
        { "data_set" : { "data" : [ "node:field-json-result" ] } },
        { "data_set" : { "data" : [ "node:field-simulation-messages" ] } },
        { "data_set" : { "data" : [ "node:field-femm-image" ], "value" : [ "" ] } }
      ]
    }
  }');
  $items['rules_set_end_time'] = entity_import('rules_config', '{ "rules_set_end_time" : {
      "LABEL" : "Set end time",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_update--simulation" : { "bundle" : "simulation" },
        "node_update--optiafpm_tool" : { "bundle" : "optiafpm_tool" },
        "node_update--userafpm_tool" : { "bundle" : "userafpm_tool" }
      },
      "IF" : [
        { "data_is" : {
            "data" : [ "node:field-simulation-status" ],
            "op" : "IN",
            "value" : { "value" : { "0" : "0", "4" : "4" } }
          }
        }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-duration:value2" ], "value" : "now" } }
      ]
    }
  }');
  $items['rules_set_in_queue'] = entity_import('rules_config', '{ "rules_set_in_queue" : {
      "LABEL" : "Set in queue",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "node", "type" : "node" } },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "node" ],
            "type" : "node",
            "bundle" : { "value" : {
                "simulation" : "simulation",
                "optiafpm_tool" : "optiafpm_tool",
                "userafpm_tool" : "userafpm_tool"
              }
            }
          }
        }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-simulation-status" ], "value" : "2" } }
      ]
    }
  }');
  $items['rules_set_start_time'] = entity_import('rules_config', '{ "rules_set_start_time" : {
      "LABEL" : "Set start time",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_update--simulation" : { "bundle" : "simulation" },
        "node_update--optiafpm_tool" : { "bundle" : "optiafpm_tool" },
        "node_update--userafpm_tool" : { "bundle" : "userafpm_tool" }
      },
      "IF" : [
        { "NOT data_is" : { "data" : [ "node-unchanged:field-simulation-status" ], "value" : "3" } },
        { "data_is" : { "data" : [ "node:field-simulation-status" ], "value" : "3" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-duration:value" ], "value" : "now" } },
        { "data_set" : { "data" : [ "node:field-duration:value2" ] } }
      ]
    }
  }');
  return $items;
}
